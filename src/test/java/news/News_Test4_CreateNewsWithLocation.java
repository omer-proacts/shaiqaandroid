package news;

import general.AndroidState;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;

/**
 * Created by omergoldstein on 2/13/17.
 */
public class News_Test4_CreateNewsWithLocation extends NewsFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();

    @Test
    public void test() throws InterruptedException {
        createNews(true,"");
        driver.navigate().back();
    }
}
