package news;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

/**
 * Created by omergoldstein on 2/12/17.
 */
public class NewsFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Common common=new Common();
    Utils Utils=new Utils();
    private final String message="news message".concat("_"+ Utils.createRandomName());
    private final String title="news title".concat("_"+ Utils.createRandomName());
    protected  void createNews(boolean withLocation,String attachment) throws InterruptedException {
        if (common.isElementPresent(driver.findElement(By.id("action_create")))) {
            driver.findElement(By.id("action_create")).click();
        }
        if (!common.isElementPresent(driver.findElement(By.id("text_community")))) {
            throw new Error("news screen not displayed");
        }

        driver.findElement(By.id("text_community")).click();
        common.selectRandomCommunity();
        if (withLocation){
            driver.findElement(By.id("toggle_location")).click();
        }
        if (attachment!=""){
            common.attachItemToObject(attachment);
        }
        driver.findElement(By.id("text_message")).sendKeys(message);
        driver.findElement(By.id("text_title")).sendKeys(title);
        driver.findElement(By.id("button_send")).click();
    }

}
