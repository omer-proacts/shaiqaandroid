package chat;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

/**
 * Created by omer on 5/11/17.
 */
public class Chat_Test3_CheckReadMessageTest extends ChatFunctions {
    Common common =new Common();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
   // @Test
    public void test() throws InterruptedException, IOException, ParseException {
        common.logOut();
        Thread.sleep(10000);
        try {
            driver.findElement(By.id("button_accept")).click();
        }
        catch (Exception e){

        }
        common.skipPhoneLogin();
        common.regularLogin(Utils.getDataFromJson("MAIL_RECIVER_USER").toString(), Utils.getDataFromJson("MAIL_FB_RECIVER_PASSWORD").toString(), false);
        while (true){
            try {
                WebElement button = driver.findElementById("button_confirm");
                if (button!=null){
                    button.click();
                }
            }
            catch (Exception e){
                break;
            }
        }
        OpenChatScreen();
        String chatButtonId="text_name";
        if (!common.isElementPresent(driver.findElement(By.id(chatButtonId)))){ return;}
        driver.findElementById(chatButtonId).click();
        Thread.sleep(10000);
        String backChatButton="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]\n";
       // driver.findElementByXPath(backChatButton).click();
       driver.navigate().back();






    }
}
