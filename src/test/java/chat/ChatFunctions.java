package chat;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;

import java.io.IOException;

/**
 * Created by omer on 5/10/17.
 */
public  class ChatFunctions  {

    Utils Utils=new Utils();
    public final String MESSAGE="chat message "+ Utils.createRandomName();
    private Common common=new Common();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    protected static String CHAT_USER ="";
    protected static String CHAT_PASSWORD ="";
    public static final String CHAT_BUTTON_XPATH ="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[3]/android.widget.CheckedTextView[1]";


    protected void sendMessage(boolean withAttachment) throws InterruptedException, IOException, ParseException {
        OpenChatScreen();
        driver.findElementById("action_create").click();
        try {
            CHAT_USER =Utils.getDataFromJson("MAIL_RECIVER_USERNAME").toString();
            CHAT_PASSWORD =Utils.getDataFromJson("MAIL_FB_RECIVER_PASSWORD").toString();
        }
        catch (Exception e){
            CHAT_USER = Utils.getDataFromJson("MAIL_RECIVER_USERNAME").toString();
            CHAT_PASSWORD =Utils.getDataFromJson("MAIL_RECIVER_PASSWORD").toString();
        }
        if (!common.isElementPresent(driver.findElement(By.id("search_src_text"))))
        {
            return;
        }
        driver.findElement(By.id("search_src_text")).sendKeys(CHAT_USER);
        String resultXpath=" //android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]\n";
        driver.findElement(By.xpath(resultXpath)).click();
        String messageXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.RelativeLayout[1]/android.widget.EditText[1]";
        driver.findElementByXPath(messageXpath).sendKeys(MESSAGE);
        if(withAttachment){
            common.attachItemToObject("gallery");
        }
        driver.findElementById("button_send_message").click();
        String backChatButton="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]\n";
        driver.findElementByXPath(backChatButton).click();
    }

    protected void OpenChatScreen() throws InterruptedException {
        common.clickOnMainMenuIcon();
        driver.findElementByXPath(CHAT_BUTTON_XPATH).click();
    }


}


