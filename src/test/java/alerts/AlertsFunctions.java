package alerts;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.Random;

/**
 * Created by omergoldstein on 2/8/17.
 */
public class AlertsFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Utils Utils=new Utils();
    private Common common=new Common();
    private final String message = "new alert".concat("_" + Utils.createRandomName());

    protected void openAlertActivityFromMenu() throws InterruptedException {
        common.clickOnMainMenuIcon();
        String alertButtonPath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[4]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(alertButtonPath)).click();
    }
    protected void selectRandomType() throws InterruptedException {
        driver.findElement(By.id("text_type")).click();

        Random rnd=new Random();
        int type=rnd.nextInt(6)+1;
        String selectedTypePath="//android.widget.ListView[1]/android.widget.LinearLayout["+type+"]";
        driver.findElement(By.xpath(selectedTypePath)).click();
    }

    protected  void createAlert(String attachmentType) throws InterruptedException {
    try {

        Common common = new Common();
        try {
            WebElement create=driver.findElement(By.id("action_create"));
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }

        driver.findElement(By.id("action_create")).click();
        //Thread.sleep(3000);
        common.scrollDown();

        if (common.isElementPresent(driver.findElement(By.id("text_message")))) {
            driver.findElement(By.id("text_message")).sendKeys(message);
            driver.findElement(By.id("text_community")).click();
            common.selectRandomCommunity();
            selectRandomType();

            common.attachItemToObject(attachmentType);
            common.scrollDown();
            driver.findElement(By.id("button_send")).click();
        }
    }
    catch (Exception e){
        System.out.println(e.getMessage());
    }
    }
}
