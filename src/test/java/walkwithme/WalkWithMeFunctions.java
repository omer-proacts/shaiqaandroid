package walkwithme;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by omer on 4/5/17.
 */
public abstract class WalkWithMeFunctions {

    public static String lastPanicTime="";
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Utils Utils=new Utils();
    private Common common=new Common();
    protected void createPanic(boolean longClick,boolean sendPanic) throws InterruptedException {
       if (!common.isElementPresent(driver.findElement(By.id("button_panic")))){return;}
        driver.findElement(By.id("button_panic")).click();
        if (!common.isElementPresent(driver.findElementById("button_walk_with_me"))){return;}
        driver.findElementById("button_walk_with_me").click();
        WebElement button= driver.findElementById("walk_with_me_btn");
        if (longClick){
            TouchAction action = new TouchAction(driver);
            action.longPress(button).release().perform();
        }
        else {
            button.click();
        }
        if (!sendPanic)
        {
            driver.navigate().back();
        }
        else {
            Thread.sleep(10000);
            Common common=new Common();
            common.clickOnMainMenuIcon();
            String reportMenu="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[5]/android.widget.CheckedTextView[1]";
            if (!common.isElementPresent(driver.findElement(By.xpath(reportMenu)))){return;}
            driver.findElement(By.xpath(reportMenu)).click();
            lastPanicTime=driver.findElementById("list_item_text_date").getText();
        }

    }

    protected boolean verifyPanic(String tgt) throws IOException, ParseException, URISyntaxException, java.text.ParseException {
         ClientResponse response;
        long millis = DateTime.parse(lastPanicTime, DateTimeFormat.forPattern("dd/MM/yy HH:mm")).getMillis();
         String output;
        response = Client.create().resource(Utils.getDataFromJson("api")+ "/walkwithme" +millis).type("application/json").header("Authorization", tgt).get(ClientResponse.class);
         output = response.getEntity(String.class);
         JSONParser parser=new JSONParser();
         JSONArray data= (JSONArray) parser.parse(output);
         JSONObject parsedJson=new JSONObject(data.get(1).toString());
         String account= parsedJson.getJSONObject("creator").getString("accountName").toString();
         if (account.equals(Utils.getDataFromJson("WALK_WITH_ME_NAME").toString())){
             return  true;
        }
        return false;
    }
  }

