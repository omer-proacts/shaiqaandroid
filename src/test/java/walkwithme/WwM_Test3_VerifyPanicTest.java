package walkwithme;

import general.Utils;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by omer on 4/5/17.
 */
public class WwM_Test3_VerifyPanicTest extends WalkWithMeFunctions {

   // @Test
    public void test() throws IOException, ParseException, URISyntaxException, java.text.ParseException {

        String tgt= "";
        try {

           tgt=Utils.loginWithApi(Utils.getDataFromJson("cw_fb_login").toString(), Utils.getDataFromJson("cw_fb_password").toString());
        }
        catch (Exception e)
        {
            tgt=Utils.loginWithApi(Utils.getDataFromJson("cw_user").toString(), Utils.getDataFromJson("cw_password").toString());
        }

           if(verifyPanic(tgt)){
               System.out.println("call recived at: "+lastPanicTime+" verified");
           }
           else {
               System.out.println("call recived at: "+lastPanicTime+" not verified");
           }

    }
}
