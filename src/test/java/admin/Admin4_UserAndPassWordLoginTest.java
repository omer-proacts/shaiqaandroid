package admin;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by omergoldstein on 2/7/17.
 */
public class Admin4_UserAndPassWordLoginTest {

    AppiumDriver driver;
    Common common=new Common();
    Utils Utils=new Utils();



    @Before
    public void before(){
        driver= AndroidState.getDriver();
    }

    @Test
    public void Test() throws InterruptedException, IOException, ParseException {
       try {
           common.skipPhoneLogin();
       }
       catch (Exception e){
           System.out.println(e.getMessage());
       }
       try {
           common.regularLogin(Utils.getDataFromJson("DEFAULT_USERNAME").toString(), Utils.getDataFromJson("DEFAULT_PASSWORD").toString(), false);
           common.setLocation();
       }
       catch (Exception e){
           System.out.println(e.getMessage());
       }

    }

}



