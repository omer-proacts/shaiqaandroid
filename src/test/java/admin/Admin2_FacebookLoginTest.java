package admin;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by omer on 5/21/17.
 */
public class Admin2_FacebookLoginTest {
    private Common common=new Common();
    private AppiumDriver driver;
    Utils Utils=new Utils();
    @Before
    public void before(){
        driver=AndroidState.getDriver();
    }


    @Test
    public void  Test() throws InterruptedException {
        try {
            driver.findElementById("button_facebook_login");
        }
        catch (Exception e){
            return;
        }

        try {
            common.skipPhoneLogin();
            common.facebookLogin(Utils.getDataFromJson("DEFAULT_FB_USERNAME"),Utils.getDataFromJson("DEFAULT_FB_PASSWORD"),false);
            common.logOut();
        }
        catch (Exception e) {

        }
        }
    }



