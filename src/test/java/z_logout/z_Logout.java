package z_logout;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;

import java.text.ParseException;

/**
 * Created by omer on 3/27/17.
 */
public class z_Logout {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();

    @Test
    public void test() throws InterruptedException, ParseException {
        driver.navigate().back();
        common.logOut();


    }

}
