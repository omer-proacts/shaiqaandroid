package crowdwisdom;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import general.Properties;
import general.Utils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by omergoldstein on 6/14/16.
 */
public  class CrowdWisdomFunctions {


    public static List<Question> questions=new ArrayList<Question>();
    private JSONParser parser = new JSONParser();
    private ClientResponse response;
    private String output;
    Utils Utils=new Utils();

    protected Question createQuestion(String tgt,String title,String answer) throws IOException, ParseException {

        general.Properties properties=new Properties();

        String SUPPORT_MESSAGE ="This is a messsage from user"+ Utils.createRandomName();
         final String jsonQuestion = String.format("{" +
                "    \"title\": \"%s\"," +
                "    \"radius\": \"5\"," +
                "    \"location\": {" +
                "        \"latitude\": %s," +
                "        \"longitude\": %s" +
                "    }}",title ,properties.DEFAULT_LAT,properties.DEFAULT_LNG);
        response = Client.create().resource(Utils.getDataFromJson("api")+ "crowdwisdom/questions").type("application/json").header("Authorization", tgt).post(ClientResponse.class, jsonQuestion);
        String output = response.getEntity(String.class);
        JSONParser jsonParser=new JSONParser();
        JSONObject data= (JSONObject) jsonParser.parse(output);
        String id= (String) data.get("id");
        String qtitle= (String) data.get("title");
        Question question=new Question();
        question.setTitle(title);
        question.setAnswer(answer);
        question.setId(id);
        return question;
    }








    public  String checkMyQuestion(String tgt,String id) throws ParseException, IOException {
        response = Client.create().resource(Utils.getDataFromJson("CROWDWISDOM")+"/"+id).header("Authorization", tgt).get(ClientResponse.class);
        output = response.getEntity(String.class);
        JSONObject jsonObject = (JSONObject) parser.parse(output);
        if (jsonObject.get("trueAnswerCounter").toString().equals("1")){
            return  "yes";
        }
        if (jsonObject.get("falseAnswerCounter").toString().equals("1")){
            return  "no";
        }
        else{
            return "canceled";
        }

    }

//    public String updateLocation(String user, String password) throws IOException, ParseException {
//        final NewCookie loginTicket = getLoginToken(user, password);
//        String tgt = getServiceTicketToken(loginTicket);
//        response = Client.create().resource(Utils.getDataFromJson("api") + "users/location").type("application/json").header("Authorization", tgt).post(ClientResponse.class, LONG_LAT);
//        response.getEntity(String.class);
//        return tgt;
//    }

    public void createAnswer(String qId, String Answer, String tgt) throws IOException, ParseException {
        final String jsonAnswer = String.format("{" +
                "    \"answer\": \"%s\"," +
                "    \"questionId\": \"%s\"," +
                "    \"comment\": \"some comment\"," +
                "    \"location\": {" +
                "        \"latitude\": 32.063863," +
                "        \"longitude\": 34.779982" +
                "    }" +
                "}", Answer, qId);
        response = Client.create().resource(Utils.getDataFromJson("api")+ "crowdwisdom/answers").type("application/json").header("Authorization", tgt).post(ClientResponse.class, jsonAnswer);
        String output = response.getEntity(String.class);
    }
}






