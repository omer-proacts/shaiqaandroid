package crowdwisdom;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.junit.After;
import org.junit.Test;


/**
 * Created by omergoldstein on 8/28/16.
 */
public class CW_T1_CrowdWisdomCreateTest extends CrowdWisdomFunctions {
    Common common=new Common();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Utils Utils=new Utils();
    @Test
    public void Test() throws InterruptedException {
       common.setLocation();
        try {
           questions.add(createQuestion(Utils.loginWithApi(Utils.getDataFromJson("cw_user"),Utils.getDataFromJson("cw_password")),"question_"+Utils.createRandomName(),"yes"));
           questions.add(createQuestion(Utils.loginWithApi(Utils.getDataFromJson("cw_user"),Utils.getDataFromJson("cw_password")),"question_"+Utils.createRandomName(),"no"));
       }
       catch (Exception e){
           System.out.print(e.getMessage());

       }
     }
    @After
    public void close(){

    }

}
