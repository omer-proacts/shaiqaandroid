package crowdwisdom;

import general.Utils;
import org.json.simple.parser.ParseException;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by omer on 3/26/17.
 */
public class CW_T3_CrowdWisdomVerifyAnswersWithApi extends CrowdWisdomFunctions {

    @Test
    public void test() throws IOException, ParseException {
            String Question1Answer="";
            String Question2Answer="";
            Utils Utils=new Utils();
            Question1Answer = checkMyQuestion(Utils.loginWithApi(Utils.getDataFromJson("cw_user").toString(), Utils.getDataFromJson("cw_password").toString()), questions.get(0).getId());
            Question2Answer = checkMyQuestion(Utils.loginWithApi(Utils.getDataFromJson("cw_user").toString(), Utils.getDataFromJson("cw_password").toString()), questions.get(1).getId());
            if (Question2Answer.equals("no")&&Question1Answer.equals("yes")) {
                System.out.println("all questions verified with api");
            }
            else {
                System.out.println("all questions not verified with api");
            }

    }
}
