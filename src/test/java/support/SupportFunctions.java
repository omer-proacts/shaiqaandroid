package support;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by omer on 5/10/17.
 */
public  class SupportFunctions {


    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Utils Utils=new Utils();
    public final String SUPPORT_MESSAGE ="This is a messsage from user"+Utils.createRandomName();
    private Common common=new Common();
    protected void CreateSupportContact() throws InterruptedException {
        common.scrollDown();
        common.clickOnMainMenuIcon();
       String aboutXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[11]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(aboutXpath)).click();
        driver.findElementById("button_contact_support").click();
        if (common.isElementPresent(driver.findElementById("text_feedback"))) {
            driver.findElementById("text_feedback").sendKeys(SUPPORT_MESSAGE);
        }
        driver.findElementById("action_send").click();
    }

    protected boolean verifySupport(String tgt) throws IOException, ParseException, URISyntaxException, java.text.ParseException {
        ClientResponse response;
        DateTime local = new DateTime();
        DateTime utc = new DateTime(System.currentTimeMillis(), DateTimeZone.UTC);
        long current = utc.getMillis();
        //long millis = DateTime.parse(Utils.getto, DateTimeFormat.forPattern("dd/MM/yy HH:mm")).getMillis();
        String output;
        response = Client.create().resource(Utils.getDataFromJson("api")+ "/walkwithme" +current).type("application/json").header("Authorization", tgt).get(ClientResponse.class);
        output = response.getEntity(String.class);
        JSONParser parser=new JSONParser();
        JSONArray data= (JSONArray) parser.parse(output);
        JSONObject parsedJson=new JSONObject(data.get(1).toString());
        String account= parsedJson.getJSONObject("creator").getString("accountName").toString();
        if (account.equals(Utils.getDataFromJson("WALK_WITH_ME_NAME").toString())){
            return  true;
        }
        return false;
    }

        }






