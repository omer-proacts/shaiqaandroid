package events;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.openqa.selenium.By;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by omergoldstein on 2/19/17.
 */
public  class EventsFunctions  {

    private Utils Utils=new Utils();
    private final String text_title = Utils.createRandomName();
    private final String event_description =  Utils.createRandomName();
    private ArrayList<String> membersArray=new ArrayList<String>();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
   // private JSONParser parser=new JSONParser();
    Common common = new Common();

    private String getNextDay() throws ParseException {
        DateTime dt = new DateTime(DateTimeZone.getDefault());
        return dt.plusDays(1).dayOfMonth().getAsText();
    }


    protected void createEvent(boolean tillTommorrow,boolean allDay,int numOfFriends,boolean withCommunity,boolean withVideo) throws ParseException, InterruptedException, IOException, org.json.simple.parser.ParseException {
        Thread.sleep(2000);
        if (common.isElementPresent(driver.findElement(By.id("action_create")))) {
            driver.findElement(By.id("action_create")).click();
        }
        //Thread.sleep(1000);
        if (tillTommorrow) {
//            List<AndroidElement> dates = driver.findElements(By.id("text_date"));
//            dates.get(1).click();
//            String nextDayXpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.DatePicker[1]/android.widget.LinearLayout[1]/android.widget.ViewAnimator[1]/android.widget.ListView[1]/android.view.View[1]/android.view.View[" + getNextDay() + "]";
//            driver.findElement(By.xpath(nextDayXpath)).click();
//            driver.findElement(By.id("android:id/button1")).click();
        }
        if (allDay) {
            driver.findElement(By.id("toggle_all_day")).click();
        }
        if (withCommunity) {
            common.scrollDown();
            driver.findElement(By.id("text_community")).click();
            common.selectRandomCommunity();
        }

        if (numOfFriends > 0) {
            if (common.isElementPresent(driver.findElement(By.id("text_members")))) {
                driver.findElement(By.id("text_members")).click();
            }
            List<String> users = Utils.getUsers();
            for (int i = 0; i < numOfFriends; i++) {
                common.addRemoveMember(users.get(i));
            }

            if (common.isElementPresent(driver.findElement(By.id(("action_accept"))))) {
                driver.findElement(By.id(("action_accept"))).click();
            }
            Thread.sleep(2000);
        }
        if (withVideo) {
            common.attachItemToObject("video");
        }
        common.scrollDown();
        if (!common.isElementPresent(driver.findElement(By.id("text_title")))){return;}
        driver.findElement(By.id("text_title")).sendKeys(text_title);
        driver.hideKeyboard();
        common.scrollDown();
        driver.findElement(By.id("text_description")).sendKeys(event_description);
        driver.hideKeyboard();
        driver.findElement(By.id("button_send")).click();
        }
}


