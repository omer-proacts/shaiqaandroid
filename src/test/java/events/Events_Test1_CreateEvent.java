package events;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;
import org.openqa.selenium.By;

import java.io.IOException;
import java.text.ParseException;

/**
 * Created by omergoldstein on 2/20/17.
 */
public class Events_Test1_CreateEvent extends EventsFunctions {


   AndroidState androidState=new AndroidState();
   AppiumDriver driver=androidState.getDriver();
   private Common common=new Common();

    @Test
    public void test() throws InterruptedException, ParseException, IOException, org.json.simple.parser.ParseException {

        common.clickOnMainMenuIcon();
        String eventButtonXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[8]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(eventButtonXpath)).click();
        createEvent(false,false,0,false,false);

    }
}
