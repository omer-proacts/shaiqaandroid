package mail;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by omergoldstein on 2/22/17.
 */
public  class MailFunctions
{
    Utils Utils=new Utils();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    private final String mailSubject="mail_test_subject_"+Utils.createRandomName();
    private final String mailBody="mail_test_body_"+Utils.createRandomName();
    private String autoCompletePath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.MultiAutoCompleteTextView[1]";
    private String bodyId = "edit_text_body";
    private String toId = "searchView";
    private String ccId="searchViewCc";
    private String bccId="searchViewBcc";
    private String ccBccArrowId="ccBccArrow";
    private String subjectId = "edit_text_subject";
    public static final String BackfromSearchXpath ="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.ImageButton[1]";

    public static void setFillDeliveryKeyboard(String fillDeliveryKeyboard) {
        MailFunctions.fillDeliveryKeyboard = fillDeliveryKeyboard;
    }

    public static String fillDeliveryKeyboard="";


    protected void sendMail(HashMap<String,String> typeAndName,String attachmentType) throws InterruptedException {
        if (!common.isElementPresent(driver.findElement(By.id("action_create")))){return;}
        else {
            driver.findElement(By.id("action_create")).click();
        }
        fillMailDetails(typeAndName,attachmentType);
    }


    protected void fillDeliveryType(List<String> delivery,String deliveryId) throws InterruptedException {
        if (!common.isElementPresent(driver.findElement(By.id(deliveryId)))) {
            return;
        }

        for (int i = 0; i < delivery.size(); i++) {
                MobileElement autoComplete= (MobileElement) driver.findElement(By.id(deliveryId));
                autoComplete.setValue(delivery.get(i));
                String selectedOption="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.view.View[1]";
                driver.findElement(By.xpath(selectedOption)).click();

        }
    }

    protected void fillMailDetails(HashMap<String,String> typeAndName,String attachmentType) throws InterruptedException {
        try {
            List<String> to = new ArrayList<String>(Arrays.asList(typeAndName.get("to").split(",")));
            fillDeliveryType(to, toId);
            Thread.sleep(5000);
        }
        catch (Exception e){
        }
        try {
            List<String> cc = new ArrayList<String>(Arrays.asList(typeAndName.get("cc").split(",")));
            driver.findElement(By.id(ccBccArrowId)).click();
            fillDeliveryType(cc, ccId);
        }
            catch (Exception e){}


        try {
            List<String> bcc = new ArrayList<String>(Arrays.asList(typeAndName.get("bcc").split(",")));
            if (!common.isElementPresent(driver.findElement(By.id(ccId)))){
             driver.findElement(By.id(ccBccArrowId)).click();
             fillDeliveryType(bcc, bccId);
            }
            else   {
                fillDeliveryType(bcc, bccId);
            }
        }
            catch (Exception e){

        }
        FillSubjectBodyAndAttachment(attachmentType);
    }

    private void FillSubjectBodyAndAttachment(String attachmentType) throws InterruptedException {
            common.scrollDown();
            driver.findElement(By.id(subjectId)).sendKeys(mailSubject);
            driver.hideKeyboard();
            driver.findElement(By.id(bodyId)).sendKeys(mailBody);
            common.attachItemToObject(attachmentType);
            driver.findElement(By.id("action_send")).click();
    }


    protected void replayOrForwardToMail(String action,HashMap<String,String> to) throws InterruptedException {
        try {
            driver.findElement(By.id("action_search")).click();
            driver.findElement(By.id("search_src_text")).sendKeys(mailSubject);
            if (common.isElementPresent(driver.findElement(By.id("text_name")))) {
                driver.findElement(By.id("text_name")).click();
            }
        }
        catch (Exception e){

        }

        if (action.toLowerCase().equals("replay")) {
            driver.findElement(By.id("action_reply")).click();
            FillSubjectBodyAndAttachment("");
        }

        if (action.toLowerCase().equals("replayall")) {
            driver.findElement(By.id("action_reply_all")).click();
            FillSubjectBodyAndAttachment("");
        }

        if (action.toLowerCase().equals("forward")) {
            driver.findElement(By.id("action_forward")).click();
            fillMailDetails(to,"");
        }

    }


    protected void deleteMail() throws InterruptedException {
        try {
            driver.findElement(By.id("action_search")).click();
            driver.findElement(By.id("search_src_text")).sendKeys(mailSubject);
            if (common.isElementPresent(driver.findElement(By.id("text_name")))) {
                driver.findElement(By.id("text_name")).click();
            }
        }
        catch (Exception e){

        }

        driver.findElement(By.id("action_delete")).click();
        driver.findElement(By.id("button1")).click();
    }


}
