package mail;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.openqa.selenium.By;

import java.io.IOException;

public class Mail_Test1_ReplayTest extends MailFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    general.Utils Utils=new Utils();

    @Test
    public void test() throws InterruptedException, IOException, ParseException {
       driver.navigate().back();
       common.logOut();
       common.skipPhoneLogin();
       common.regularLogin(Utils.getDataFromJson("MAIL_RECIVER2_USER").toString(), Utils.getDataFromJson("MAIL_RECIVER2_PASSWORD").toString(), false);
        try {
            common.cancelQuestions();
        }
        catch (Exception e){

        }
        Thread.sleep(500);
        common.clickOnMainMenuIcon();
        String mailMenuXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[10]/android.widget.CheckedTextView[1]";
        if (common.isElementPresent(driver.findElementByXPath(mailMenuXpath))) {
            driver.findElement(By.xpath(mailMenuXpath)).click();
        }
       replayOrForwardToMail("replay",null);
     }

}
