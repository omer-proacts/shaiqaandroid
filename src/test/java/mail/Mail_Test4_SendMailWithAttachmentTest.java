package mail;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by omergoldstein on 2/23/17.
 */
public class Mail_Test4_SendMailWithAttachmentTest extends MailFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    private HashMap<String,String> recipients=new HashMap<String, String>();


    @Before
    public void setUp() throws IOException, ParseException {
        recipients.put("to", "Omer Goldstein");
    }

    @Test
    public void test() throws InterruptedException, IOException, ParseException {
        Common common=new Common();
        driver.navigate().back();
        driver.navigate().back();
        driver.navigate().back();
        sendMail(recipients,"gallery");

    }
}
