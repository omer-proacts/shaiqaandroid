package mail;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by omer on 5/8/17.
 */
public class Mail_Test0_SendMailToCoupleRecipientsTest extends MailFunctions {

    AppiumDriver driver;
    private Common common=new Common();
    private HashMap<String,String> recipients=new HashMap<String, String>();
    private String recipient1;
    private String recipient2;
    general.Utils Utils=new Utils();


    @Before
   public void setUp() throws IOException, ParseException {
       driver=AndroidState.getDriver();
       recipient1 = Utils.getDataFromJson("MAIL_RECIVER_USERNAME").toString();
       recipient2 = Utils.getDataFromJson("MAIL_RECIVER2_USERNAME").toString();
       recipients.put("to", recipient1 + "," + recipient2);
   }


   @Test
    public void test() throws InterruptedException, IOException, ParseException {
        driver.navigate().back();
        driver.navigate().back();
        Thread.sleep(10000);
        common.clickOnMainMenuIcon();
        String mailMenuXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[10]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(mailMenuXpath)).click();
        sendMail(recipients,"");

    }
}
