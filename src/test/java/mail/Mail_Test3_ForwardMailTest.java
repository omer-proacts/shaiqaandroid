package mail;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

public class Mail_Test3_ForwardMailTest extends MailFunctions {


    private HashMap<String,String> recipients=new HashMap<String, String>();
    private String recipient1;
    private String recipient2;
    private String recipient3;

    @Before
    public void setUp() throws IOException, ParseException {
        recipients.put("to", "Raj C");
        recipients.put("cc","Omer Goldstein");
        recipients.put("bcc","ere e");
    }

    @Test
    public void test() throws InterruptedException, IOException, ParseException {
        replayOrForwardToMail("forward", recipients);

    }
}
