package general;


import crowdwisdom.CrowdWisdomFunctions;
import crowdwisdom.Question;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.html5.Location;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;

/**
 * Created by omergoldstein on 2/27/17.
 */
public class Common {


    private Utils Utils=new Utils();

    public static void setCameraId(String cameraId) {
        Common.cameraId = cameraId;
    }

    public static String cameraId="";

    public static void setDoneButtonId(String doneButtonId) {
        Common.doneButtonId = doneButtonId;
    }

    public static String doneButtonId="";

    public static void setRecentImageId(String recentImageId) {
        Common.recentImageId = recentImageId;
    }

    public static String recentImageId="";
    public static String menuButton="android.widget.ImageButton";

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    Properties properties = new Properties();
    public HashMap<String, String> phoneKeys = new HashMap<String, String>();
    //TODO: create validation for created entities

    public Common() {
        if (phoneKeys.size() > 0) {
            return;
        }
        {
            phoneKeys.put("0", "7");
            phoneKeys.put("1", "8");
            phoneKeys.put("11", "222");
            phoneKeys.put("12", "228");
            phoneKeys.put("2", "9");
            phoneKeys.put("2", "9");
            phoneKeys.put("3", "10");
            phoneKeys.put("4", "11");
            phoneKeys.put("5", "12");
            phoneKeys.put("6", "13");
            phoneKeys.put("7", "14");
            phoneKeys.put("8", "15");
            phoneKeys.put("9", "16");
            phoneKeys.put("a", "29");
            phoneKeys.put("b", "30");
            phoneKeys.put("c", "31");
            phoneKeys.put("d", "32");
            phoneKeys.put("e", "33");
            phoneKeys.put("f", "34");
            phoneKeys.put("g", "35");
            phoneKeys.put("h", "36");
            phoneKeys.put("i", "37");
            phoneKeys.put("j", "38");
            phoneKeys.put("k", "39");
            phoneKeys.put("l", "40");
            phoneKeys.put("m", "41");
            phoneKeys.put("n", "42");
            phoneKeys.put("o", "43");
            phoneKeys.put("p", "44");
            phoneKeys.put("p", "44");
            phoneKeys.put("q", "45");
            phoneKeys.put("r", "46");
            phoneKeys.put("s", "47");
            phoneKeys.put("t", "48");
            phoneKeys.put("u", "49");
            phoneKeys.put("v", "50");
            phoneKeys.put("w", "51");
            phoneKeys.put("x", "52");
            phoneKeys.put("y", "53");
            phoneKeys.put("z", "54");
            phoneKeys.put(" ", "62");
        }


    }

    public void attachItemToObject(String attachmentType) throws InterruptedException {
        if (attachmentType.length() != 0) {
            if (attachmentType == "audio") {
                driver.findElement(By.id("button_record_sound")).click();
                sleep(3000);
                driver.findElement(By.id("button_record_sound")).click();
            }
            if (attachmentType == "gallery") {
                attachItem("gallery");
            }
            if (attachmentType == "camera") {
                attachItem("camera");
            }
            if (attachmentType == "video") {
                   attachItem("video");
            }

        }
    }


    private void clickFolderOnFileMaager(String name) throws InterruptedException {
       try {
           if (!isElementPresent(driver.findElement(By.id("com.asus.filemanager:id/file_list_item_name")))){
               throw new Error("folders not shown");
           }
           List<Object> items=driver.findElements(By.id("com.asus.filemanager:id/file_list_item_name"));
           for (Object o :items ) {
               WebElement folder = (WebElement) o;
               if (folder.getText().equals(name)) {
                   folder.click();
                   Thread.sleep(2000);
               }

           }
       }
       catch (Exception e){
           System.out.println(e.getMessage());
       }
      }


    private void attachItem(String type) throws InterruptedException {
        String galleryXpath = "//android.widget.ListView[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]";
        String cameraXpath = "//android.widget.ListView[1]/android.widget.LinearLayout[2]/android.widget.RelativeLayout[1]/android.widget.TextView[1]";
        String openFrom = "android.widget.ImageButton";
        String videoXPath="//android.widget.ListView[1]/android.widget.LinearLayout[3]/android.widget.RelativeLayout[1]/android.widget.TextView[1]";
        String fileManagerMenuXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.view.View[1]/android.widget.ImageButton[1]";
        String sdCardPath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.LinearLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.TextView[1]";

        driver.findElement(By.id("action_add_attachment")).click();
        if (type=="video"){
            driver.findElementByXPath(videoXPath).click();
            if (!isElementPresent(driver.findElementById(cameraId+"/shutter_button"))){return;}
            driver.findElementById(cameraId+"/shutter_button").click();
            Thread.sleep(3000);
            if (!isElementPresent(driver.findElementById(cameraId+"/shutter_button"))){return;}
            driver.findElementById(cameraId+"/shutter_button").click();
            Thread.sleep(3000);
            if (!isElementPresent(driver.findElementById(cameraId+"/"+doneButtonId))){return;}
            driver.findElementById(cameraId+"/"+doneButtonId).click();
        }

        if (type == "gallery") {
            driver.findElement(By.xpath(galleryXpath)).click();
            if (isElementPresent(driver.findElement(By.className(openFrom)))) {
                driver.findElement(By.className(openFrom)).click();
            }
            if (isElementPresent(driver.findElementById("android:id/title"))) {
                for (Object o : driver.findElementsById("android:id/title")) {
                    WebElement foler = (WebElement) o;
                    if (foler.getText().equals("Recent")) {
                        foler.click();
                        break;
                    }
                }
            }
//            clickFolderOnFileMaager("DCIM");
//            clickFolderOnFileMaager("Camera");


//            if (isElementPresent(driver.findElementById("com.asus.filemanager:id/file_list_item_name"))) {
//                List<WebElement> items = driver.findElementsById("com.asus.filemanager:id/file_list_item_name");
//                items.get(new Random().nextInt(items.size())).click();
//            }
            if (isElementPresent(driver.findElementByClassName("android.widget.ImageView"))) {
                List<WebElement> items = driver.findElementsById(recentImageId);
                items.get(new Random().nextInt(items.size())).click();
            }
        }
        if (type == "camera") {
            driver.findElementByXPath(cameraXpath).click();
            try {
                if (!isElementPresent(driver.findElementById(cameraId+"/shutter_button"))){return;}
                driver.findElementById(cameraId+"/shutter_button").click();
                if (!isElementPresent(driver.findElementById(cameraId+"/"+doneButtonId))){return;}
                driver.findElementById(cameraId+"/"+doneButtonId).click();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public boolean validateEntityCreation(String type, String name) {
        List<AndroidElement> names = driver.findElements(By.id("text_name"));
        for (AndroidElement textName : names) {
            if (textName.getText().equalsIgnoreCase(name)) {
                textName.click();
                break;
            }
        }
        if (type == "alert" || type == "news") {
            if (driver.findElement(By.id("text_content")).getText().equalsIgnoreCase(name)) {
                driver.navigate().back();
                return true;
            }
        }
        if (type == "event") {
            String eventTitleXpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.ScrollView[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]";
            if (driver.findElement(By.xpath(eventTitleXpath)).getText().equalsIgnoreCase(name)) {
                driver.navigate().back();
                return true;
            }
        }
        return false;
    }

    public void pressSingleCharEveryTime(String name, By elementFinder) {
        driver.findElement(elementFinder).click();
        for (int i = 0; i < name.length(); i++) {
            String currentChar = name.substring(i, i + 1).toLowerCase();
            driver.findElement(elementFinder).sendKeys(currentChar);

        }
    }

    public void scrollDown() throws InterruptedException {

        try {
            Dimension dimensions = driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.5;
            int scrollStart = screenHeightStart.intValue();
            Double screenHeightEnd = dimensions.getHeight() * 0.2;
            int scrollEnd = screenHeightEnd.intValue();
            //driver.(0, scrollStart, 0, scrollEnd,5000);
            TouchAction action = new TouchAction(driver).longPress(20,0).moveTo(20, 1000).release();
            action.perform();
           }
           catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    public void logOut() throws InterruptedException {
            clickOnMainMenuIcon();
            driver.findElement(By.id("navigation_drawer_header_account_name")).click();
            driver.findElement(By.id("button_logout")).click();
            driver.findElement(By.id("button1")).click();
            Thread.sleep(30000);
            try {
                if (isElementPresent(driver.findElementById("button_accept"))) {
                    driver.findElementById("button_accept").click();
                }
            } catch (Exception e) {
                System.out.println("EULA not display");
            }
        }


    public void addRemoveMember(String memberName) throws InterruptedException {
        driver.findElement(By.id("action_search")).click();
        driver.findElement(By.id("search_src_text")).sendKeys(memberName);
        String results = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]";
        try {
            driver.findElement(By.xpath(results)).click();
        }
        catch (Exception e)
        {

        }
        driver.findElement(By.id("search_close_btn")).click();
    }

    public void selectRandomCommunity() throws InterruptedException {

         Random rand = new Random();
        int community = rand.nextInt(9);
        if (community == 0) {
            community++;
        }
        String selectedcommunityPath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[" + community + "]";
        String defaultSelectedcommunityPath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[1]";
        try {
            if (isElementPresent(driver.findElement(By.xpath(selectedcommunityPath)))) {
                driver.findElement(By.xpath(selectedcommunityPath)).click();
            }
        } catch (Exception e) {
            driver.findElement(By.xpath(defaultSelectedcommunityPath)).click();
        }
    }


    public void clickOnMainMenuIcon() throws InterruptedException {
          if (isElementPresent(driver.findElement(By.className("android.widget.ImageButton")))) {
               driver.findElement(By.className("android.widget.ImageButton")).click();
           }

    }

    public boolean isElementPresent(WebElement element)  {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOf(element));

        }
        catch (Exception e){
            return false;
        }

        return true;
    }


    public void googleLogin() throws InterruptedException {
        String googleButtonXpath = "//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.Button[1]";

        driver.findElementByXPath(googleButtonXpath).click();

        try {
               String accountId = "com.google.android.gms:id/account_display_name";
                if (isElementPresent(driver.findElementById(accountId))) {
                    driver.findElementById(accountId).click();
                }

            } catch (Exception e) {
        }

            try {
                Thread.sleep(1000);
                cancelQuestions();
            }
            catch (Exception e){

            }
            logOut();
            try {
                if (isElementPresent(driver.findElement(By.id("button_accept")))) {
                    driver.findElement(By.id("button_accept")).click();
                }
            }
            catch (Exception e){

            }
        }




    public void facebookLogin(String userName, String password, boolean replayToQuestions) throws InterruptedException, IOException, ParseException {
        //Set contextNames = driver.getContextHandles();
        try {
            driver.findElementById("button_facebook_login").click();

        }
    catch (Exception e){
            return;
    }
        try {

                       driver.findElementById("button_facebook_login").click();
                       java.lang.Thread.sleep(5000);

                   try {
                       driver.findElementById("button1").click();
                   } catch (Exception e) {

                   }
                   try {
                       String emailXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]/android.widget.EditText[1]";
                       String passwordXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]/android.widget.EditText[2]";
                       String loginXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[3]/android.widget.Button[1]\n";
                       String continueAsXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[2]/android.view.View[1]/android.widget.Button[1]";
                       String agreeXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[2]/android.view.View[1]/android.widget.Button[1]";
                       if (isElementPresent(driver.findElement(By.xpath(emailXpath)))) {
                           driver.findElement(By.xpath(emailXpath)).sendKeys(userName);
                           driver.findElement(By.xpath(passwordXpath)).sendKeys(password);
                           driver.findElement(By.xpath(loginXpath)).click();
                           }
                           sleep(15000);
                       try
                       {
                           if (isElementPresent(driver.findElement(By.xpath(continueAsXpath)))) {
                               driver.findElement(By.xpath(continueAsXpath)).click();
                           }
                       }
                       catch (Exception e){}

                       try
                        {
                            if (isElementPresent(driver.findElement(By.xpath(agreeXpath)))) {
                                driver.findElement(By.xpath(agreeXpath)).click();
                            }
                        }
                        catch (Exception e){}
                       try
                       {
                           sleep(15000);
                           driver.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.widget.Button[1]")).click();
                       }

                       catch (Exception e){

                       }
                       try {
                           driver.findElement(By.xpath("//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.webkit.WebView[1]/android.webkit.WebView[1]/android.view.View[4]/android.view.View[2]/android.view.View[2]/android.widget.Button[1]")).click();
                       }
                       catch (Exception e){

                       }

                       try {


                               driver.findElement(By.id("action_create")).click();

                       }
                       catch (Exception e) {

                       }
                   } catch (Exception e) {
                   }

                   try {
                       driver.findElement(By.name("continue")).click();
                   } catch (Exception e) {
                       try {
                           driver.findElementByXPath("//button[@name='__CONFIRM__']").click();
                       }
                       catch (Exception bc){

                       }

                   }
               }
               catch (Exception e){


               }
        //driver.context(contextNames.toArray()[0].toString());
        try {
            sleep(2000);
            driver.findElementById("field_org_code").sendKeys(Utils.getDataFromJson("orgenization_code"));
            driver.hideKeyboard();
            driver.findElementById("button_org_code_confirm").click();
            sleep(2000);
            driver.findElementById("field_community").click();
            String communityXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.LinearLayoutCompat[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.CheckedTextView[3]";
            driver.findElementByXPath(communityXpath).click();
            driver.findElementById("button_save").click();

        }
        catch (Exception e){

        }
        if (!replayToQuestions){
            try {
               cancelQuestions();
            }
            catch (Exception e){

            }
        }
        else{
           answerQuestions();
        }

            }


    public void skipPhoneLogin(){
        Common common=new Common();
        try {
               driver.findElement(By.id("button_username_login")).click();
       }
        catch (Exception e){

        }
    }
    public  void regularLogin(String userName, String password, boolean replayToQuestions) throws InterruptedException {
        driver.findElement(By.id("edittext_username")).sendKeys(userName);
        driver.hideKeyboard();
        driver.findElement(By.id("edittext_password")).sendKeys(password);
        driver.hideKeyboard();
        driver.findElement(By.id("button_login")).click();
        if (!replayToQuestions) {
            try {
                cancelQuestions();
            } catch (Exception e) {

            }
        } else {
              answerQuestions();
        }



    }

    public void answerQuestions() throws InterruptedException {
        Thread.sleep(10000);
        try {
            List<Question> questions= CrowdWisdomFunctions.questions;

            while(true) {
                String questionText = driver.findElementById("text_message").getText();
                if (questions.get(0).getTitle().equals(questionText)){
                    if (questions.get(0).getAnswer().equals("yes")){
                        driver.findElementById("button_confirm").click();
                        questionText = driver.findElementById("text_message").getText();

                    }
                    if (questions.get(0).getAnswer().equals("no")){
                        driver.findElementById("button_dismiss").click();
                        questionText = driver.findElementById("text_message").getText();

                    }
                }
                if (questions.get(1).getTitle().equals(questionText)){
                    if (questions.get(1).getAnswer().equals("yes")){
                        driver.findElementById("button_confirm").click();
                        questionText = driver.findElementById("text_message").getText();
                        return;
                    }
                    if (questions.get(1).getAnswer().equals("no")){
                        driver.findElementById("button_dismiss").click();

                        return;
                    }
                }
                else {
                    driver.findElementById("button_dismiss").click();
                }
            }


        }
        catch (Exception e){


        }
    }
     public void setLocation() throws InterruptedException {
        // if (isElementPresent(driver.findElementById("com.mediapplications.shai.shaiapp.qa1:id/touch_wrapper_view"))) {
             driver.setLocation(new Location(properties.DEFAULT_LAT, properties.DEFAULT_LNG,properties.DEFAULT_ALTITUDE));
             sleep(5000);
         //}
     }



    public void cancelQuestions() throws InterruptedException {
        String cancel = "android.widget.ImageButton";
        String noButton="android.widget.Button";
            try {
                Thread.sleep(30000);
                if (isElementPresent(driver.findElement(By.className(cancel)))) {
                    driver.findElement(By.className(cancel)).click();
                }
            }
                catch(Exception e){
                    System.out.println(e.getMessage());
                }
                try {
                if (isElementPresent(driver.findElement(By.className(noButton)))){
                  WebElement no=(WebElement)driver.findElements(By.className(noButton)).get(1);
                  no.click();
                }
            }
            catch (Exception e){
                //throw new Error(e.getMessage());
            }

         }
       }






