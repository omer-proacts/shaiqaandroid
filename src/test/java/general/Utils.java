package general;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created by omergoldstein on 2/7/17.
 */
public  class Utils {

    private static JSONParser parser = new JSONParser();
    private static ClientResponse response;
    private static java.lang.String output;
    String jsonFile=new File("src/main/resources/"+System.getProperty("env")+".json").getAbsolutePath();
    public   NewCookie getLoginToken(java.lang.String user, java.lang.String password) {
        try {
            java.lang.String QA1_REST_LOGIN_BODY = "username=" + user + "&password=" + password + "&rememberMe=true&deviceId=NA";
            WebResource webResource = Client.create().resource(getDataFromJson("REST_LOGIN"));
            response = webResource.post(ClientResponse.class, QA1_REST_LOGIN_BODY);
            output = response.getEntity(java.lang.String.class);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }

        final List<NewCookie> cookies = response.getCookies();
        for (NewCookie cookie : cookies) {
            if (cookie.getName().equals("tgt")) {
                return cookie;
            }
        }
        throw new IllegalStateException("tgt not found in response " + response.toString());

    }

    public  java.lang.String getServiceTicketToken(NewCookie loginTicket) {

        try {
            WebResource webResource = Client.create().resource(getDataFromJson("service_ticket"));
            WebResource.Builder builder = webResource.getRequestBuilder();
            builder = builder.cookie(loginTicket);
            ClientResponse response = builder.post(ClientResponse.class, getDataFromJson("services"));
            java.lang.String output = response.getEntity(java.lang.String.class);
            JSONObject jsonObject = (JSONObject) parser.parse(output);
            Object data = jsonObject.get("data");
            HashMap dataObject = (JSONObject) parser.parse(data.toString());
            HashMap keyObject = (HashMap) dataObject.get("serviceTickets");
            java.lang.String val= (java.lang.String) keyObject.get(getDataFromJson("CAS_SERVICE_NAME")).toString();
            return val;

        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return null;

    }
    public  java.lang.String loginWithApi(java.lang.String user, java.lang.String password){
        Cookie loginCookie= getLoginToken(user,password);
        java.lang.String serviceTicket=getServiceTicketToken((NewCookie) loginCookie);
        return serviceTicket;
    }

 public  java.lang.String createRandomName() {
        final java.lang.String lexicon = "ABCDEFGHIJKLMNOPQRSTUVWXYZ12345674890";
        final java.util.Random rand = new java.util.Random();
        final Set<String> identifiers = new HashSet<String>();
        StringBuilder builder = new StringBuilder();
        while (builder.toString().length() == 0) {
            int length = rand.nextInt(5) + 5;
            for (int i = 0; i < length; i++)
                builder.append(lexicon.charAt(rand.nextInt(lexicon.length())));
            if (identifiers.contains(builder.toString()))
                builder = new StringBuilder();
        }
        return builder.toString();

    }
    public  java.lang.String getDataFromJson(java.lang.String name) throws IOException, ParseException{
        JSONParser parser = new JSONParser();
        try {
            Object file = parser.parse(new FileReader(jsonFile));
            JSONObject data = (JSONObject) file;
            return (java.lang.String) data.get(name);
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
        return "";
    }
    public  List<java.lang.String> getUsers() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object file = parser.parse(new FileReader(jsonFile));
        JSONObject data = (JSONObject) file;
        JSONArray users= (JSONArray) data.get("users");
        List<java.lang.String> usersList=new ArrayList<java.lang.String>();
        for (int i=0;i<users.size();i++){
            JSONObject user= (JSONObject) users.get(i);
            usersList.add(user.get("name").toString());
        }
        return usersList;

    }


}
