package general;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Created by omergoldstein on 2/27/17.
 */
public class AndroidState  {

  public static AndroidDriver driver;
  public static AndroidDriver getDriver(){
        return  driver;
    }
  String qaFile=new File("src/main/resources/app/"+System.getProperty("env")+".apk").getAbsolutePath();
  Utils utils=new Utils();

//  public static void downLoadAppFromUrl() throws IOException {
//        String appUrl="https://s3-eu-west-1.amazonaws.com/shai-android-ee62b234be350736a9df35bf8cf1c17403170443/qa1/current.apk";
//        FileOutputStream fos = new FileOutputStream(appDestination);
//        try {
//            URL website = new URL(appUrl);
//            ReadableByteChannel rbc;
//            rbc = Channels.newChannel(website.openStream());
//            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
//            fos.close();
//            rbc.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//    }

    public  void   setUp() throws IOException, ParseException {

        String loginActivity="com.mediapplications.shai.app.setup.LoginActivity";
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,System.getProperty("driver.avd"));
        capabilities.setCapability(MobileCapabilityType.VERSION,System.getProperty("driver.version"));
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, false);
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        capabilities.setCapability(AndroidMobileCapabilityType.AUTO_GRANT_PERMISSIONS, true);
//      capabilities.setCapability("automationName", "uiautomator2");
        capabilities.setCapability(MobileCapabilityType.APP, qaFile);
        capabilities.setCapability("appPackage", utils.getDataFromJson("launchPackge"));
        capabilities.setCapability("appActivity",  loginActivity);
        try {
            driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
            if ( !System.getProperty("driver.version").equals("5.1")){
                Common.setCameraId("com.android.camera2:id");
                Common.setDoneButtonId("done_button");
                Common.setRecentImageId("com.android.documentsui:id/icon_thumb");
            }
            if (System.getProperty("driver.version").equals("5.1"))
            {
                Common.setCameraId("com.android.camera:id");
                Common.setDoneButtonId("btn_done");
                Common.setRecentImageId("com.android.documentsui:id/icon_mime");

            }
        }
        catch (Exception e){
            throw new Error("Error Message: "+e.getMessage());

        }

    }

    }
