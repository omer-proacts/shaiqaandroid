package groups;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Created by omergoldstein on 2/17/17.
 */
public class Groups_Test2_RemoveMember extends GroupsFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    @Test
    public void test() throws InterruptedException {
        openGroup();
        driver.findElement(By.id("action_edit")).click();
        common.addRemoveMember(membersArray.get(0));
        driver.findElement(By.id(("action_accept"))).click();

    }
}
