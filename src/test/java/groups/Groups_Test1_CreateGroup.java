package groups;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.openqa.selenium.By;

import java.io.IOException;

/**
 * Created by omergoldstein on 2/12/17.
 */
public class Groups_Test1_CreateGroup  extends GroupsFunctions {


    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    @Test
    public void test() throws InterruptedException, IOException, ParseException {
        common.clickOnMainMenuIcon();
        String groupMenu="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[7]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(groupMenu)).click();
        createGroup(3);

    }

}

