package groups;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by omergoldstein on 2/12/17.
 */
public  class GroupsFunctions   {

    Utils Utils=new Utils();
    public  final String GROUP_NAME="group_"+Utils.createRandomName();
    public  final ArrayList<String> membersArray=new ArrayList<String>();
    private final String NEW_POST = "new post " + Utils.createRandomName();
    private JSONParser parser=new JSONParser();
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();

    protected void openGroup() throws InterruptedException {
        String groupXpath="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[1]/android.widget.TextView[1]";
        driver.findElement(By.id("action_search")).click();
        driver.findElement(By.id("search_src_text")).sendKeys(GROUP_NAME);
        driver.findElement(By.xpath(groupXpath)).click();
    }


    public void createGroup(int numOfFriends) throws InterruptedException, IOException, ParseException {

            driver.findElement(By.id("action_add")).click();
            driver.findElement(By.id("text_group_name")).sendKeys(GROUP_NAME);
            int index;
            Random random=new Random();

            List<String> users =  Utils.getUsers();

           // membersArray.add(users.get(users.size()-1).toString());
            for (int i=0;i<numOfFriends;i++){
                    membersArray.add(users.get(i));
                }


            Common common=new Common();
            for (String member : membersArray) {
                common.addRemoveMember(member);
            }
            driver.findElement(By.id(("action_accept"))).click();



        }

        public void deleteGroup(){

            driver.findElement(By.id("action_delete")).click();
            driver.findElement(By.id("button1")).click();
        }



       public void createPost(boolean withLocation,String attachmentType) throws InterruptedException {
           driver.findElement(By.id("action_create")).click();
           Common common=new Common();
           common.attachItemToObject(attachmentType);
           if(withLocation){
               driver.findElement(By.id("toggle_location")).click();
           }
           driver.findElement(By.id("text_message")).sendKeys(NEW_POST);
           driver.findElement(By.id("button_send")).click();
       }
}
