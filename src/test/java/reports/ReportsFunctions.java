package reports;

import general.AndroidState;
import general.Common;
import general.Utils;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;

/**
 * Created by omergoldstein on 2/12/17.
 */
public class ReportsFunctions {

    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();
    Utils utils=new Utils();
    private final String message="new report".concat("_"+ utils.createRandomName());



    protected void createPanicReport(String type, boolean clickOnSEndButton) throws InterruptedException {
        if (!common.isElementPresent(driver.findElement(By.id("button_panic")))){return;}
        driver.findElement(By.id("button_panic")).click();
        driver.findElement(By.id("button_send_panic")).click();
        if (type=="medical"){
            driver.findElement(By.id("button_call_type_medical")).click();
        }
        if (type=="violence") {
            driver.findElement(By.id("button_call_type_violence")).click();
        }
        if (clickOnSEndButton){
            driver.findElement(By.id("button_send")).click();
        }
        else {
            Thread.sleep(10000);
        }
    }
    protected void cancelPanic() throws InterruptedException {
        if (!common.isElementPresent(driver.findElement(By.id("button_panic")))){return;}
        driver.findElement(By.id("button_panic")).click();
        driver.findElement(By.id("button_send_panic")).click();
        Thread.sleep(1000);
        driver.navigate().back();
    }


    protected  void createReport(String attachmentType) throws InterruptedException {

        driver.findElement(By.id("action_create")).click();
        Thread.sleep(3000);
        if (!common.isElementPresent(driver.findElement(By.id("text_message")))){return;}
        driver.findElement(By.id("text_message")).sendKeys(message);
        if (attachmentType.length()!=0)
        {
            if (attachmentType=="audio"){
                driver.findElement(By.id("button_record_sound")).click();
                Thread.sleep(3000);
                driver.findElement(By.id("button_record_sound")).click();
            }
        }
        driver.findElement(By.id("button_send")).click();

    }
}
