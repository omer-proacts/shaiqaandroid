package reports;

import general.AndroidState;
import general.Common;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * Created by omergoldstein on 2/12/17.
 */
public class Reports_Test3_CreateReport extends ReportsFunctions {


    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    private Common common=new Common();

    @Test
    public void test() throws InterruptedException {
        Thread.sleep(5000);
        common.clickOnMainMenuIcon();
        String reportMenu="//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.support.v4.widget.DrawerLayout[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]/android.support.v7.widget.LinearLayoutCompat[5]/android.widget.CheckedTextView[1]";
        driver.findElement(By.xpath(reportMenu)).click();
        createReport("");

    }

}
