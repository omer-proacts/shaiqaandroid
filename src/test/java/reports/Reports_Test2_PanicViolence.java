package reports;

import org.junit.Test;

/**
 * Created by omergoldstein on 2/28/17.
 */
public class Reports_Test2_PanicViolence extends ReportsFunctions {

    @Test
    public void test() throws InterruptedException {
        Thread.sleep(5000);
        createPanicReport("violence",true);
    }
}
