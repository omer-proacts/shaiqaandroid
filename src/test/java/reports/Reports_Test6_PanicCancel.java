package reports;

import general.AndroidState;
import io.appium.java_client.AppiumDriver;
import org.junit.Test;

/**
 * Created by omer on 5/10/17.
 */
public class Reports_Test6_PanicCancel extends ReportsFunctions {
    AndroidState androidState=new AndroidState();
    AppiumDriver driver=androidState.getDriver();
    @Test
    public void test() throws InterruptedException {
        cancelPanic();
    }
}
